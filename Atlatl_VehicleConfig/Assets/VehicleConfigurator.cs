﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// enum of all the parts involved, so we dont have string typos
/// </summary>
public enum CarParts
{
    Body,
    FrontArmor,
    BackArmor,
    FrontBumper,
    BackBumper,
    FrontLights,
    BackLights,
    TopArmor,
    Wheels,
    Suspension,
    Spoiler,
    Motor,
    Decoration,
    Brakes,
    Count
}

/// <summary>
/// helper class to hold a pair of strings, used for car part rules management currently
/// </summary>
public class StringTuple
{
    public string a;
    public string b;

    public StringTuple(string a, string b)
    {
        this.a = a;
        this.b = b;
    }

    /// <summary>
    /// check if these string pairs match in either order
    /// </summary>
    public bool checkMatch(string first, string second)
    {
        if ((first.Contains(a) && second.Contains(b) ) ||
            (first.Contains(b) && second.Contains(a)))
            return true;
        return false;
    }
}

/// <summary>
/// the workhorse of this demo
/// handles configuration, setting up the car for driving, etc
/// </summary>
public class VehicleConfigurator : MonoBehaviour {
    //couple simple action callbacks, currently just used to play some sounds
    public System.Action OnInvalidClick;
    public System.Action OnPartChange;
    public System.Action OnPartSelected;
    public System.Action OnPartUnselected;
    public System.Action OnLoadVehicle;
    public System.Action OnStartVehicle;

    //link to the ui manager since we let this class change part labels
    public UIManager uiManager;

    //stores all the parts that make up our car
    List<BodyPart> parts = new List<BodyPart>();

    //these 4 objects specify where the wheels go0
    public GameObject rrWheel, rlWheel, frWheel, flWheel;

    //materials for each of the 3 part "brands"
    public Material mil1Mat;
    public Material mil2Mat;
    public Material mil3Mat;

    //shaders used when selecting things
    public Shader standardShader;
    public Shader selectedShader;

    //this object is used as a template for creating our body part gameobjects
    public GameObject baseGO;

    //we'll check that any part combos in here arent allowed to exist
    public List<StringTuple> invalidCombos = new List<StringTuple>();
    
    //flag when we're in an invalid state so we cant change to other parts or save until its fixed
    public bool currentlyInvalid = false;

    BodyPart lastSelected = null; //so we know which one to unselect when selecting a new piece

	void Start ()
    {
        //left mesh components on the base object to make it easier to position it in the scene
        //but we dont want it to actually render on this gameobject at runtime, so remove them now
        Destroy(GetComponent("MeshRenderer"));
        Destroy(GetComponent("MeshFilter"));

        //standardShader = Shader.Find("Standard");
        //selectedShader = Shader.Find("Custom/Selected");

        //load part paths, etc into our data structure
        //in a real project, this would probably be stored in some sort of config so it could be edited
        //without having to edit source code, but this works fine for now
        BodyPart body = createBodyPart(CarParts.Body);
        body.partPaths.Add("ArtAssets/Models/Mil_1_Body_1");
        body.partPaths.Add("ArtAssets/Models/Mil_2_Body_2");
        body.partPaths.Add("ArtAssets/Models/Mil_3_Body_3");
        parts.Add(body);

        BodyPart frontArmor = createBodyPart(CarParts.FrontArmor);
        frontArmor.partPaths.Add("ArtAssets/Models/Mil_1_Armor_F_1");
        frontArmor.partPaths.Add("ArtAssets/Models/Mil_2_Armor_F_2");
        frontArmor.partPaths.Add("ArtAssets/Models/Mil_3_Armor_F_3");
        parts.Add(frontArmor);

        BodyPart backArmor = createBodyPart(CarParts.BackArmor);
        backArmor.partPaths.Add("ArtAssets/Models/Mil_1_Armor_B_1");
        backArmor.partPaths.Add("ArtAssets/Models/Mil_2_Armor_B_2");
        backArmor.partPaths.Add("ArtAssets/Models/Mil_3_Armor_B_3");
        parts.Add(backArmor);

        BodyPart frontBumper = createBodyPart(CarParts.FrontBumper);
        frontBumper.partPaths.Add("ArtAssets/Models/Mil_1_Bumper_F_1");
        frontBumper.partPaths.Add("ArtAssets/Models/Mil_2_Bumper_F_2");
        frontBumper.partPaths.Add("ArtAssets/Models/Mil_3_Bumper_F_3");
        parts.Add(frontBumper);

        BodyPart backBumper = createBodyPart(CarParts.BackBumper);
        backBumper.partPaths.Add("ArtAssets/Models/Mil_1_Bumper_B_1");
        backBumper.partPaths.Add("ArtAssets/Models/Mil_2_Bumper_B_2");
        backBumper.partPaths.Add("ArtAssets/Models/Mil_3_Bumper_B_3");
        parts.Add(backBumper);

        BodyPart frontLights = createBodyPart(CarParts.FrontLights);
        frontLights.partPaths.Add("ArtAssets/Models/Mil_1_Light_F_1");
        frontLights.partPaths.Add("ArtAssets/Models/Mil_2_Light_F_2");
        frontLights.partPaths.Add("ArtAssets/Models/Mil_3_Light_F_3");
        parts.Add(frontLights);

        BodyPart backLights = createBodyPart(CarParts.BackLights);
        backLights.partPaths.Add("ArtAssets/Models/Mil_1_Light_B_1");
        backLights.partPaths.Add("ArtAssets/Models/Mil_2_Light_B_2");
        backLights.partPaths.Add("ArtAssets/Models/Mil_3_Light_B_3");
        parts.Add(backLights);

        BodyPart topArmor = createBodyPart(CarParts.TopArmor);
        topArmor.partPaths.Add("ArtAssets/Models/Mil_1_Back_1");
        topArmor.partPaths.Add("ArtAssets/Models/Mil_2_Back_2");
        topArmor.partPaths.Add("ArtAssets/Models/Mil_3_Back_3");
        parts.Add(topArmor);

        BodyPart wheel1 = createBodyPart(CarParts.Wheels);
        wheel1.partPaths.Add("ArtAssets/Models/Mil_1_Wheel_1");
        wheel1.partPaths.Add("ArtAssets/Models/Mil_2_Wheel_2");
        wheel1.partPaths.Add("ArtAssets/Models/Mil_3_Wheel_3");

        BodyPart wheel2 = createBodyPart(CarParts.Wheels);
        wheel2.partPaths.Add("ArtAssets/Models/Mil_1_Wheel_1");
        wheel2.partPaths.Add("ArtAssets/Models/Mil_2_Wheel_2");
        wheel2.partPaths.Add("ArtAssets/Models/Mil_3_Wheel_3");

        BodyPart wheel3 = createBodyPart(CarParts.Wheels);
        wheel3.partPaths.Add("ArtAssets/Models/Mil_1_Wheel_1");
        wheel3.partPaths.Add("ArtAssets/Models/Mil_2_Wheel_2");
        wheel3.partPaths.Add("ArtAssets/Models/Mil_3_Wheel_3");

        BodyPart wheel4 = createBodyPart(CarParts.Wheels);
        wheel4.partPaths.Add("ArtAssets/Models/Mil_1_Wheel_1");
        wheel4.partPaths.Add("ArtAssets/Models/Mil_2_Wheel_2");
        wheel4.partPaths.Add("ArtAssets/Models/Mil_3_Wheel_3");

        //position the wheels since they're offset
        wheel1.transform.position = rrWheel.transform.position;
        wheel2.transform.position = rlWheel.transform.position;
        wheel3.transform.position = frWheel.transform.position;
        wheel4.transform.position = flWheel.transform.position;

        wheel1.transform.rotation = rrWheel.transform.rotation;
        wheel2.transform.rotation = rlWheel.transform.rotation;
        wheel3.transform.rotation = frWheel.transform.rotation;
        wheel4.transform.rotation = flWheel.transform.rotation;

        wheel1.hubPos = "rr";
        wheel2.hubPos = "rl";
        wheel3.hubPos = "fr";
        wheel4.hubPos = "fl";

        parts.Add(wheel1);
        parts.Add(wheel2);
        parts.Add(wheel3);
        parts.Add(wheel4);

        BodyPart suspension = createBodyPart(CarParts.Suspension);
        suspension.partPaths.Add("ArtAssets/Models/Mil_1_Suspension_1");
        suspension.partPaths.Add("ArtAssets/Models/Mil_2_Suspension_2");
        suspension.partPaths.Add("ArtAssets/Models/Mil_3_Suspension_3");
        parts.Add(suspension);

        BodyPart spoiler = createBodyPart(CarParts.Spoiler);
        spoiler.partPaths.Add("ArtAssets/Models/Mil_1_Spoiler_1");
        spoiler.partPaths.Add("ArtAssets/Models/Mil_2_Spoiler_2");
        spoiler.partPaths.Add("ArtAssets/Models/Mil_3_Spoiler_3");
        parts.Add(spoiler);

        BodyPart motor = createBodyPart(CarParts.Motor);
        motor.partPaths.Add("ArtAssets/Models/Mil_1_Motor_1");
        motor.partPaths.Add("ArtAssets/Models/Mil_2_Motor_2");
        motor.partPaths.Add("ArtAssets/Models/Mil_3_Motor_3");
        parts.Add(motor);

        BodyPart decoration = createBodyPart(CarParts.Decoration);
        decoration.partPaths.Add("ArtAssets/Models/Mil_1_Deco_1");
        decoration.partPaths.Add("ArtAssets/Models/Mil_2_Deco_2");
        decoration.partPaths.Add("ArtAssets/Models/Mil_3_Deco_3");
        parts.Add(decoration);

        BodyPart brakes1 = createBodyPart(CarParts.Brakes);
        brakes1.partPaths.Add("ArtAssets/Models/Mil_1_Brake_1");
        brakes1.partPaths.Add("ArtAssets/Models/Mil_2_Brake_2");
        brakes1.partPaths.Add("ArtAssets/Models/Mil_3_Brake_3");

        BodyPart brakes2 = createBodyPart(CarParts.Brakes);
        brakes2.partPaths.Add("ArtAssets/Models/Mil_1_Brake_1");
        brakes2.partPaths.Add("ArtAssets/Models/Mil_2_Brake_2");
        brakes2.partPaths.Add("ArtAssets/Models/Mil_3_Brake_3");

        BodyPart brakes3 = createBodyPart(CarParts.Brakes);
        brakes3.partPaths.Add("ArtAssets/Models/Mil_1_Brake_1");
        brakes3.partPaths.Add("ArtAssets/Models/Mil_2_Brake_2");
        brakes3.partPaths.Add("ArtAssets/Models/Mil_3_Brake_3");

        BodyPart brakes4 = createBodyPart(CarParts.Brakes);
        brakes4.partPaths.Add("ArtAssets/Models/Mil_1_Brake_1");
        brakes4.partPaths.Add("ArtAssets/Models/Mil_2_Brake_2");
        brakes4.partPaths.Add("ArtAssets/Models/Mil_3_Brake_3");

        //position the brakes since they're offset
        brakes1.transform.position = rrWheel.transform.position;
        brakes2.transform.position = rlWheel.transform.position;
        brakes3.transform.position = frWheel.transform.position;
        brakes4.transform.position = flWheel.transform.position;

        brakes1.transform.rotation = rrWheel.transform.rotation;
        brakes2.transform.rotation = rlWheel.transform.rotation;
        brakes3.transform.rotation = frWheel.transform.rotation;
        brakes4.transform.rotation = flWheel.transform.rotation;

        brakes1.hubPos = "rr";
        brakes2.hubPos = "rl";
        brakes3.hubPos = "fr";
        brakes4.hubPos = "fl";

        parts.Add(brakes1);
        parts.Add(brakes2);
        parts.Add(brakes3);
        parts.Add(brakes4);

        //find the parts that are of the same type (ie all wheels) and link them together
        combineMatchingParts();

        //put us in our default config
        initializeFirstState();

        //clean up ui state
        uiManager.setSelectionUIVisiblility(false);
        uiManager.setPartLabelText("None");

        //set any invalid body part combinations
        invalidCombos.Add(new StringTuple("Mil_1_Body", "Mil_2_Armor_B"));
        invalidCombos.Add(new StringTuple("Mil_3_Deco", "Mil_3_Armor_F"));
        invalidCombos.Add(new StringTuple("Mil_3_Deco", "Mil_2_Body"));
    }

    /// <summary>
    /// true if any part is selected, otherwise false
    /// </summary>
    public bool isPartSelected()
    {
        for (int i = 0; i < parts.Count; i++)
            if (parts[i].Selected)
                return true;
        return false;
    }

    /// <summary>
    /// we have a few multi-part elements that belong together, like 4 wheels
    /// find and link them
    /// </summary>
    void combineMatchingParts()
    {
        for(int i = 0; i < parts.Count; i++)
        {
            for(int j = 0; j < parts.Count; j++)
            {
                if (i == j) continue; //dont match ourself

                if(parts[j].partType == parts[i].partType) //match found, link it
                    parts[i].linkedParts.Add(parts[j]);
            }
        }
    }

    /// <summary>
    /// create a new body part of the given type
    /// </summary>
    BodyPart createBodyPart(CarParts part)
    {
        GameObject go = GameObject.Instantiate(baseGO);
        go.name = part.ToString();
        //align to our parent
        go.transform.position = transform.position;
        go.transform.rotation = transform.rotation;
        go.transform.parent = transform;

        //toss on the body part class and set it up
        BodyPart bp = go.AddComponent<BodyPart>();
        bp.partType = part;
        bp.parentConfig = this;

        return bp;
    }

    /// <summary>
    /// loop over our parts and make sure they're initialized properly
    /// if we wanted something other than part 0 for everything, it would be done here
    /// </summary>
    void initializeFirstState()
    {
        for (int i = 0; i < parts.Count; i++)
        {
            changePart(parts[i].partType, 0);
            parts[i].updateCollision();
        }
    }

    /// <summary>
    /// return all the parts of a given type
    /// </summary>
    public List<BodyPart> getPartsByType(CarParts type)
    {
        List<BodyPart> results = new List<BodyPart>();
        for(int i = 0; i < parts.Count; i++)
        {
            if (parts[i].partType == type)
                results.Add(parts[i]);
        }

        return results;
    }

    /// <summary>
    /// shifts the all parts of the specified type's index in the given direction to change it
    /// SelectedIndex loads up meshes as needed rather than having them all pulled into the scene to keep things cleaner in the editor
    /// </summary>
    public void changePart(CarParts partType, int dir)
    {
        List<BodyPart> results = getPartsByType(partType);
        for (int i = 0; i < results.Count; i++)
        {
            results[i].SelectedIndex += dir;

            //update the label
            if (lastSelected != null)
                uiManager.setPartLabelText(lastSelected.partType + " - " + lastSelected.SelectedPartName);

            //make sure this part is allowed
            checkPartRules();
        }

        if (OnPartChange != null && dir != 0)
            OnPartChange();
    }

    /// <summary>
    /// move the part type index of selected part in the given direction
    /// </summary>
    public void changePart(int dir)
    {
        if (lastSelected != null)
            lastSelected.SelectedIndex += dir;
        uiManager.setPartLabelText(lastSelected.partType + " - " + lastSelected.SelectedPartName);
        checkPartRules();

        if (OnPartChange != null && dir != 0)
            OnPartChange();
    }

    /// <summary>
    /// Check to see if the selected part is compatible with other selected parts per the rules
    /// </summary>
    bool checkPartRules()
    {
        //loop over our rule pairs, see if we have parts selected that match them, if we do, flag as invalid
        for(int i = 0; i < invalidCombos.Count; i++)
        {
            for(int j = 0; j < parts.Count; j++)
            {
                if(invalidCombos[i].checkMatch(lastSelected.SelectedPartName, parts[j].SelectedPartName))
                {
                    setInvalidPart();
                    return false;
                }
            }
        }
        currentlyInvalid = false;
        return true;
    }

    /// <summary>
    /// set the part to invalid, and turn the part label red as well
    /// </summary>
    void setInvalidPart()
    {
        currentlyInvalid = true;

        if (lastSelected != null)
            lastSelected.setInvalid();
        uiManager.setPartLabelColor(Color.red);
        
    }

    void Update () {

        //dont give a chance to change part selection while part is invalid
        if(!currentlyInvalid)
        {
            if (InputManager.GetMouseButtonDown(0))
            {
                //cast a ray to find which piece we hit
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo = new RaycastHit();
                if (Physics.Raycast(ray, out hitInfo))
                {
                    //simple check to make sure we hit a bodypart, might need something more robust in a full game
                    if (hitInfo.collider.tag == "BodyPart")
                    {
                        //changePart(hitInfo.collider.name, 1);
                        uiManager.setSelectionUIVisiblility(true);



                        //select the body part we hit
                        BodyPart bp = hitInfo.collider.gameObject.GetComponent("BodyPart") as BodyPart;
                        if (bp != null)
                        {
                            if (bp != lastSelected)
                            {
                                //unselect previous
                                if (lastSelected != null)
                                    lastSelected.Selected = false;

                                bp.Selected = true;
                                lastSelected = bp;
                                uiManager.setPartLabelText(bp.name + " - " + bp.SelectedPartName);
                                checkPartRules();

                                if (OnPartSelected != null)
                                    OnPartSelected();
                            }
                        }
                    }
                    else
                        unselect();
                }
                else //unselect
                    unselect();
            }
        }
        else if (InputManager.GetMouseButtonDown(0))
        {
            InvalidClick();
        }

    }

    /// <summary>
    /// handle clicking when we dont have a valid object
    /// </summary>
    public void InvalidClick()
    {
        if(currentlyInvalid)
            if (OnInvalidClick != null)
                OnInvalidClick();
    }

    /// <summary>
    /// load a config by name from the db, if that name exists
    /// </summary>
    public void load(string name)
    {
        List<ConfigData> data = DBAccess.getConfigByName(name);
        if (data.Count > 0)
        {
            unselect();
            for (int i = 0; i < parts.Count; i++)
                parts[i].SelectedIndex = data[0].getID(parts[i].partType);
        }
        if (OnLoadVehicle != null)
            OnLoadVehicle();
    }

    /// <summary>
    /// save the current config to the db
    /// </summary>
    public void save(string name)
    {
        ConfigData data = new ConfigData();
        data.Name = name;
        //collect all our necessary part info
        for (int i = 0; i < parts.Count; i++)
            data.configIDs.Add(new PartData(parts[i].partType, parts[i].SelectedIndex));
        
        DBAccess.addDataToDB(data);
    }

    /// <summary>
    /// generate a random config
    /// </summary>
    public void randomize()
    {
        unselect(); //make sure we clear out any selection colors/shader changes to start

        for(int i = 0; i < parts.Count; i++)
        {
            //just a simple randomize, but since we have certain combos that arent valid, keep retrying until a valid part comes up
            //theoretically this could cause a very long lag pause, but it is statistically impossible to happen by a noticable amount
            do
            {
                lastSelected = parts[i];
                parts[i].SelectedIndex = Random.Range(0, parts[i].partPaths.Count - 1);
            }
            while (!checkPartRules());
        }

        unselect();

        if (OnLoadVehicle != null)
            OnLoadVehicle();
    }

    /// <summary>
    /// clears out any selection effects
    /// </summary>
    void unselect()
    {
        if(lastSelected != null)
            lastSelected.Selected = false;
        lastSelected = null;

        uiManager.setPartLabelText("None");
        uiManager.setPartLabelColor(Color.white);
        uiManager.setSelectionUIVisiblility(false);

        if (OnPartUnselected != null)
            OnPartUnselected();
    }

    /// <summary>
    /// switch to driving mode.  does not currently have a way to return to configuring
    /// </summary>
    public void startDriving()
    {
        //play the door animation on the garage
        GameObject garage = GameObject.Find("garage");
        Animation[] anim = garage.GetComponentsInChildren<Animation>();
        for(int i = 0; i < anim.Length; i++)
            anim[i].Play();

        //disable the door collision so we can drive through it
        GameObject.Find("doorCol").SetActive(false);

        //remove all the mesh collisions, they're not needed for driving around and would just cause issues
        for(int i = 0; i < parts.Count; i++)
        {
            Destroy(parts[i].gameObject.GetComponent("MeshCollider"));
        }

        //switch the camera to follow mode
        OrbitCamera cam = GameObject.Find("Main Camera").GetComponent("OrbitCamera") as OrbitCamera;
        cam.orbitCam = false;

        //add in the required physics bits
        Rigidbody rb = transform.gameObject.AddComponent<Rigidbody>();
        rb.mass = 1500;
        rb.interpolation = RigidbodyInterpolation.Interpolate;

        WheelCollider wc1 = rrWheel.AddComponent<WheelCollider>();
        WheelCollider wc2 = rlWheel.AddComponent<WheelCollider>();
        WheelCollider wc3 = frWheel.AddComponent<WheelCollider>();
        WheelCollider wc4 = flWheel.AddComponent<WheelCollider>();

        WheelFrictionCurve wfc = wc1.sidewaysFriction;
        wfc.stiffness = 0.75f;
        wc1.sidewaysFriction = wfc;
        wc2.sidewaysFriction = wfc;
        wc3.sidewaysFriction = wfc;
        wc4.sidewaysFriction = wfc;

        CarController controller = GetComponent("CarController") as CarController;
        controller.axles[0].leftWheel = wc4;
        controller.axles[0].rightWheel = wc3;
        controller.axles[1].leftWheel = wc2;
        controller.axles[1].rightWheel = wc1;

        //pass over the wheels and brakes
        for(int i = 0; i < parts.Count; i++)
        {
            if(parts[i].partType == CarParts.Wheels)
            {
                if (parts[i].hubPos == "rr")
                    controller.axles[1].RightMesh = parts[i].transform.gameObject;
                else if (parts[i].hubPos == "rl")
                    controller.axles[1].LeftMesh = parts[i].transform.gameObject;
                else if (parts[i].hubPos == "fr")
                    controller.axles[0].RightMesh = parts[i].transform.gameObject;
                else if (parts[i].hubPos == "fl")
                    controller.axles[0].LeftMesh = parts[i].transform.gameObject;
            }
            else if(parts[i].partType == CarParts.Brakes)
            {
                if (parts[i].hubPos == "rr")
                    controller.axles[3].RightMesh = parts[i].transform.gameObject;
                else if (parts[i].hubPos == "rl")
                    controller.axles[3].LeftMesh = parts[i].transform.gameObject;
                else if (parts[i].hubPos == "fr")
                    controller.axles[2].RightMesh = parts[i].transform.gameObject;
                else if (parts[i].hubPos == "fl")
                    controller.axles[2].LeftMesh = parts[i].transform.gameObject;
            }
        }

        //enable user control
        controller.canDrive = true;

        if (OnStartVehicle != null)
            OnStartVehicle();
    }
}

