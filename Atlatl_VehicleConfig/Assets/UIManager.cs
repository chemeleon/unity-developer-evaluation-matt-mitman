﻿using UnityEngine;
using System.Collections.Generic;

//if this was a real project I'd spend some time making the ui suit the user's custom resolution better
//ie buttons scale so they dont get miniscule at high res
//position panels just off screen so they appear at a consistent timing
//etc
//that just seems beyond the scope of this test

/// <summary>
/// UI Manager provides a layer of separation between ui elements and game elements, so you don't have to dig around the
/// unity editor finding all your controls if you change function names or the like elsewhere
/// </summary>
public class UIManager : MonoBehaviour {

    //store references to ui elements we care about hiding/unhiding, etc
    public GameObject prevButton;
    public GameObject nextButton;
    public GameObject gameUIPanel;
    public GameObject loadWindow;
    public GameObject saveWindow;
    public GameObject loadButton;

    //when true, input manager won't let the scene interact with things, leaving just the ui accessible
    public bool isModalCurrently = false;

    //cache ref to the vehicle config since a lot of our ui functions need to call stuff on it
    public VehicleConfigurator vehicleConfig;

    /// <summary>
    /// bring the load dialog to the front and hide the game elements
    /// </summary>
    public void displayLoad()
    {
        isModalCurrently = true;
        hideGameUI();
        loadButton.SetActive(true);

        List<string> names = DBAccess.getConfigNames();
        UILabel lbl = GameObject.Find("loadListText").GetComponent("UILabel") as UILabel;
        if (names.Count == 0)
        {
            lbl.text = "No saved configs";
            loadButton.SetActive(false);
        }
        else
            lbl.text = names[0];

        UIPopupList list = GameObject.Find("loadList").GetComponent("UIPopupList") as UIPopupList;
        list.items = names;
    }

    /// <summary>
    /// change to the next part
    /// </summary>
    public void nextPart()
    {
        vehicleConfig.changePart(1);
    }

    /// <summary>
    /// change to the previous part
    /// </summary>
    public void prevPart()
    {
        vehicleConfig.changePart(-1);
    }

    /// <summary>
    /// bring up the save dialog and hide the game element, but only if we dont have an invalid part currently set
    /// </summary>
    public void displaySave()
    {
        if (!vehicleConfig.currentlyInvalid)
        {
            isModalCurrently = true;
            hideGameUI();
        }
        else
            vehicleConfig.InvalidClick();
    }

    /// <summary>
    /// hide the game ui elements
    /// </summary>
    void hideGameUI()
    {
        setSelectionUIVisiblility(false);
        gameUIPanel.SetActive(false);
    }

    /// <summary>
    /// return from the load or save dialog but dont do anything else
    /// </summary>
    void cancelLoadSave()
    {
        displayGameUI();
    }

    /// <summary>
    /// bring the game ui elements back
    /// </summary>
    void displayGameUI()
    {
        setSelectionUIVisiblility(vehicleConfig.isPartSelected());
        gameUIPanel.SetActive(true);
        isModalCurrently = false;
    }

    /// <summary>
    /// save to the db with the user's specified name
    /// </summary>
    public void save()
    {
        UIInput input = GameObject.Find("saveFilename").GetComponent("UIInput") as UIInput;
        //make sure the user put in a valid choice
        if (input.text == null || input.text == "")
        {
            input.text = "gimme a name";
        }
        else
        {
            vehicleConfig.save(input.text);
            displayGameUI();
        }
    }

    public void load()
    {
        UIPopupList list = GameObject.Find("loadList").GetComponent("UIPopupList") as UIPopupList;
        if (list.selection != null && list.selection != "")
            vehicleConfig.load(list.selection);
        displayGameUI();
    }

    /// <summary>
    /// randomize the vehicle
    /// </summary>
    public void randomize()
    {
        vehicleConfig.randomize();
    }

    /// <summary>
    /// show/hide just the change part arrows
    /// </summary>
    public void setSelectionUIVisiblility(bool visible)
    {
        if (nextButton != null)
            nextButton.SetActive(visible);
        if (prevButton != null)
            prevButton.SetActive(visible);
    }

    /// <summary>
    /// set the text on the part label, and reset color to white
    /// </summary>
    /// <param name="text"></param>
    public void setPartLabelText(string text)
    {
        GameObject label = GameObject.Find("partLabel");
        if (label != null)
        {
            UILabel lbl = label.GetComponent("UILabel") as UILabel;
            lbl.text = text;
            lbl.color = Color.white;
        }
    }

    /// <summary>
    /// set the color of the part label text
    /// </summary>
    public void setPartLabelColor(Color color)
    {
        GameObject label = GameObject.Find("partLabel");
        if (label != null)
        {
            UILabel lbl = label.GetComponent("UILabel") as UILabel;
            lbl.color = color;
        }
    }

    /// <summary>
    /// change to driving mode
    /// </summary>
    public void startDriving()
    {
        hideGameUI();
        vehicleConfig.startDriving();
    }
}
