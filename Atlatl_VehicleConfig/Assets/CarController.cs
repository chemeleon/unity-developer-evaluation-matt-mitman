﻿using UnityEngine;
using System.Collections.Generic;

// based on http://docs.unity3d.com/Manual/WheelColliderTutorial.html
/// <summary>
/// Simple drivable car setup using WheelColliders, with two extra axles to handle positioning the brake models
/// </summary>
public class CarController : MonoBehaviour {

    public bool canDrive = false;

    public List<AxleInfo> axles;
    public float maxMotorTorque;
    public float maxSteeringAngle;

	void FixedUpdate ()
    {
        //false when in edit mode
        if (canDrive)
        {
            //wasd by default
            float motor = maxMotorTorque * InputManager.GetAxis("Vertical");
            float steering = maxSteeringAngle * InputManager.GetAxis("Horizontal");

            for (int i = 0; i < axles.Count; i++)
            {
                //turn the axles that can turn
                if (axles[i].steering)
                {
                    if(axles[i].leftWheel != null)
                        axles[i].leftWheel.steerAngle = steering;
                    if(axles[i].rightWheel != null)
                        axles[i].rightWheel.steerAngle = steering;
                }

                //power the axles that can move the car
                if (axles[i].motor)
                {
                    axles[i].leftWheel.motorTorque = motor;
                    axles[i].rightWheel.motorTorque = motor;
                }

                Vector3 position;
                Quaternion rotation;
                //if the whole axle can spin, set position and rotation to the mesh based on the wheelcollider's transform
                if (axles[i].canSpin)
                {
                    axles[i].leftWheel.GetWorldPose(out position, out rotation);
                    axles[i].LeftMesh.transform.position = position;
                    axles[i].LeftMesh.transform.rotation = rotation * axles[i].leftMeshBaseRotation;

                    axles[i].rightWheel.GetWorldPose(out position, out rotation);
                    axles[i].RightMesh.transform.position = position;
                    axles[i].RightMesh.transform.rotation = rotation * axles[i].rightMeshBaseRotation;
                }
                else //we only want to position, and possibly steer the mesh
                {
                    //this is currently slightly hacky, due to assuming it should look 2 axles earlier in the list to find the actual wheel objects
                    //works fine for this case, but should be more general purpose in a real game
                    axles[i-2].rightWheel.GetWorldPose(out position, out rotation);
                    axles[i].RightMesh.transform.position = position;

                    axles[i - 2].leftWheel.GetWorldPose(out position, out rotation);
                    axles[i].LeftMesh.transform.position = position;

                    if (axles[i].steering)//just turn with steering, currently used for the brake meshes
                    {
                        Quaternion baseRot = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
                        axles[i].LeftMesh.transform.rotation = baseRot * Quaternion.Euler(0, axles[i - 2].leftWheel.steerAngle, 0) * axles[i].leftMeshBaseRotation;
                        axles[i].RightMesh.transform.rotation = baseRot * Quaternion.Euler(0, axles[i - 2].rightWheel.steerAngle, 0) * axles[i].rightMeshBaseRotation;
                    }

                }
            }
        }
	}
}

/// <summary>
/// Helper class to store data per axle
/// </summary>
[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    GameObject leftMesh;
    GameObject rightMesh;
    public Quaternion leftMeshBaseRotation;
    public Quaternion rightMeshBaseRotation;
    public bool motor;
    public bool steering;
    public bool canSpin;

    public GameObject LeftMesh
    {
        get { return leftMesh; }
        set
        {
            leftMesh = value;
            //cache the transform since we'll need to add it in to the wheel collider's transform to properly position the mesh
            leftMeshBaseRotation = value.transform.rotation;
        }
    }

    public GameObject RightMesh
    {
        get { return rightMesh; }
        set
        {
            rightMesh = value;
            //cache the transform since we'll need to add it in to the wheel collider's transform to properly position the mesh
            rightMeshBaseRotation = value.transform.rotation;
        }
    }
}