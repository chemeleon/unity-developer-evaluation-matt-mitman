﻿using UnityEngine;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using System;

//based on guide at http://answers.unity3d.com/questions/743400/database-sqlite-setup-for-unity.html
public class DBAccess : MonoBehaviour {

    static string conn;
	static IDbConnection dbconn;
    static IDbCommand dbcmd;

    /// <summary>
    /// like it says, connects to db so we dont duplicate the connection string all over
    /// </summary>
    static void connectToDB()
    {
        if (dbconn == null)
        {
            conn = "URI=file:" + Application.dataPath + "/VehicleConfig.db"; //Path to database.

            dbconn = (IDbConnection)new SqliteConnection(conn);
            dbconn.Open(); //Open connection to the database.
        }
    }

    /// <summary>
    /// queries the DB for all the names of saved configs
    /// </summary>
    public static List<string> getConfigNames()
    {
        try
        {
            connectToDB();

            dbcmd = dbconn.CreateCommand();
            //just grab all the values in the Name column from the vehicleconfig table
            string sqlQuery = "SELECT Name FROM VehicleConfig";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();
            List<string> names = new List<string>();
            while (reader.Read())
            {
                string n = reader.GetString(0);
                if(n != null && n.Trim().Length > 0)
                    names.Add(n);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            closeDB();

            return names;
        }
        catch(Exception e)
        {
            Debug.LogError("DB isn't happy: " + e.Message);
            return new List<string>();
        }
    }

    /// <summary>
    /// adds a config to the db
    /// </summary>
    /// <param name="data"></param>
    public static void addDataToDB(ConfigData data)
    {
        try
        {
            List<string> existingNames = getConfigNames();

            connectToDB();
            dbcmd = dbconn.CreateCommand();

            string cols = "Name,";
            string vals = "'" + data.Name + "',";
            string sqlQuery = "";

            //if the name already exists, update this row, otherwise add new one
            if (existingNames.Contains(data.Name))
            {
                sqlQuery = "UPDATE VehicleConfig SET ";
                for (int i = 0; i < data.configIDs.Count; i++)
                {
                    //grab the column data, in one string this time since the columns and values go together
                    if (!cols.Contains(data.configIDs[i].partType.ToString())) //we have a few multiple parts, but only store one id for each (ie wheels)
                        cols += data.configIDs[i].partType.ToString() + "=" + data.configIDs[i].id.ToString() + ",";
                }

                cols = cols.Substring(0, cols.Length - 1);
                sqlQuery += cols + "WHERE Name=" + data.Name;
            }
            else //new name
            {
                for (int i = 0; i < data.configIDs.Count; i++)
                {
                    //collect our column data, together at once to ensure it stays matched up
                    if (!cols.Contains(data.configIDs[i].partType.ToString())) //we have a few multiple parts, but only store one id for each (ie wheels)
                    {
                        cols += data.configIDs[i].partType.ToString() + ",";
                        vals += data.configIDs[i].id.ToString() + ",";
                    }
                }

                //strip the last comma off
                cols = cols.Substring(0, cols.Length - 1);
                vals = vals.Substring(0, vals.Length - 1);


                sqlQuery = "INSERT INTO VehicleConfig(" + cols + ")";
                sqlQuery += " VALUES( " + vals + ")";
            }


            dbcmd.CommandText = sqlQuery;
            dbcmd.ExecuteNonQuery();

            closeDB();
        }
        catch(Exception e)
        {
            Debug.LogError("Db isn't happy: " + e.Message);
        }
    }

    /// <summary>
    /// get the config by name.  let it return a list<> just in case we want to allow multiple same names later on
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static List<ConfigData> getConfigByName(string name)
    {
        List<ConfigData> results = new List<ConfigData>();

        try
        {
            connectToDB();

            dbcmd = dbconn.CreateCommand();
            string cols = "";
            for (int i = 0; i < (int)CarParts.Count; i++)
                cols += ((CarParts)i).ToString() + ",";

            //strip the last comma off
            cols = cols.Substring(0, cols.Length - 1);
            //grab all column data in the order of the enum, from the vehicleconfig table for the row(s) with the matching name
            string sqlQuery = "SELECT " + cols + " FROM VehicleConfig WHERE Name='" + name + "'";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();
            
            while (reader.Read())
            {
                ConfigData data = new ConfigData();
                data.Name = name;
                for (int i = 0; i < (int)CarParts.Count; i++)
                    data.configIDs.Add(new PartData((CarParts)i, reader.GetInt32(i)));

                results.Add(data);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;

            closeDB();
        }
        catch(Exception e)
        {
            Debug.LogError("DB isnt happy: " + e.Message);
        }
        return results;
    }

    /// <summary>
    /// close out our db connection
    /// </summary>
    static void closeDB()
    {
        dbconn.Close();
        dbconn = null;
        //forcing a gc collect, because an asnwer on stackoverflow said the db isnt actually closed until gc collect is run
        //and we want to be sure we're done before we risk the user hitting it again
        //may not be needed, but doesnt do any harm here, unless this was being hit during gameplay, since that could cause a hiccup (which it isnt so far)
        GC.Collect();
    }
}
