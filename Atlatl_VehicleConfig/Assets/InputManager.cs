﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Wrapper class for input
/// in this case just used to prevent improper interaction between 3d scene and ui, based on a tip 
/// from http://gamedev.stackexchange.com/questions/73881/prevent-gui-clickthrough
/// </summary>
public class InputManager : MonoBehaviour {

    
    public static bool GetMouseButtonDown(int button)
    {
        UIManager ui = GameObject.Find("UIManager").GetComponent("UIManager") as UIManager;
        if (UICamera.hoveredObject == null && !ui.isModalCurrently) //block control when in modal mode
        {
            return Input.GetMouseButtonDown(button);
        }
        return false;
    }

    public static bool GetMouseButton(int button)
    {
        UIManager ui = GameObject.Find("UIManager").GetComponent("UIManager") as UIManager;
        if (UICamera.hoveredObject == null && !ui.isModalCurrently) //block control when in modal mode
        {
            return Input.GetMouseButton(button);
        }
        return false;
    }

    public static float GetAxis(string axis)
    {
        UIManager ui = GameObject.Find("UIManager").GetComponent("UIManager") as UIManager;
        if (ui.isModalCurrently)
            return 0;

        return Input.GetAxis(axis);
    }
}
