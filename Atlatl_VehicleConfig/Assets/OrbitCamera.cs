﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Primarily an orbit camera class
/// also has ability to turn into a follow cam by setting orbitCam false, for use when driving around
/// </summary>
public class OrbitCamera : MonoBehaviour {

    public GameObject target; //the object to track

    public bool orbitCam = true; //when false, becomes follow cam

    //the angles that control our spherical position
    public float theta = 0;
    public float phi = 0;
    public float maxPhi = -6; //-90 is looking straight down
    
    public float distance = 5; //distance from the camera to the target, ie zoom
    public float currentDistance; //used for smoothing

    public float minDistance = 1; //closest we can zoom in

    public float maxDistance = 20; //farthest we can zoom out

    //scalar values to control mapping from mouse to scene movement speed
    public float mouseSpeed = 1;
    public float zoomSpeed = 1;

    //options to invert camera inputs
    public bool invertZoomDir = false;
    public bool invertYAxis = false;

    public float catchupRate = 0.5f; //used to give the follow cam some springiness

    void Start()
    {
        //make sure these match so we dont jump around on start
        currentDistance = distance;
    }

	void Update ()
    {
        //update whichever cam mode is active
        if (orbitCam)
            updateOrbitCam();
        else
            updateFollowCam();
    }

    void updateOrbitCam()
    {
        if (InputManager.GetMouseButton(1)) //right mouse button is held down?
        {
            //move camera with mouse movement, taking into account speed var and y axis inversion
            float xAxis = Input.GetAxis("Mouse X") * mouseSpeed;
            float yAxis = Input.GetAxis("Mouse Y") * (invertYAxis ? -1 : 1) * mouseSpeed;

            theta += xAxis;
            phi += yAxis;

            //clamp phi (up/down) to just a hair under -90 and maxPhi (ground level) so we stop moving at the poles
            //clamping to less than 90 because at exactly 90 the up direction of the camera changes
            phi = Mathf.Clamp(phi, -89.99f, maxPhi);
        }

        //let the scrollwheel change our camera distance
        float zoomDelta = InputManager.GetAxis("Mouse ScrollWheel") * (invertZoomDir ? 1 : -1);
        distance = Mathf.Clamp(distance + zoomDelta * zoomSpeed, minDistance, maxDistance);

        //convert our control vars to actual transform values for unity
        Quaternion rotDegQuat = Quaternion.Euler(phi, theta, 0);
        Vector3 dir = rotDegQuat * new Vector3(0, 0, 1);

        //perform a raycast to see if there are any objects between our camera's desired position and the target
        //if there are, reduce distance to put it in front
        Ray ray = new Ray(target.transform.position, dir);
        RaycastHit hitInfo = new RaycastHit();
        float dist = distance;
        int layerMask = 1 << 8;
        if (Physics.Raycast(ray, out hitInfo, 100f, layerMask))
            dist = Mathf.Min((hitInfo.point - target.transform.position).magnitude - 0.1f, dist);
        
        //smooth the transition over a couple frames so it doesnt risk being a massive pop
        currentDistance += (dist - currentDistance) * 0.2f;
        transform.position = dir * currentDistance + target.transform.position;
        transform.LookAt(target.transform);
    }

    void updateFollowCam()
    {
        //allow zooming with mouse wheel
        float zoomDelta = InputManager.GetAxis("Mouse ScrollWheel") * (invertZoomDir ? 1 : -1);
        distance = Mathf.Clamp(distance + zoomDelta * zoomSpeed, minDistance, maxDistance);

        //find the "ideal" position, then move to it over a couple frames, so we get a slight rubber band effect
        //that bit of delay gives a better sense of vehicle movement when turning
        Vector3 pos = target.transform.position;
        pos += target.transform.rotation * (Vector3.Normalize(new Vector3(0, 1, -2)) * distance);

        Vector3 delta = pos - transform.position;

        transform.position += delta * catchupRate;
        transform.LookAt(target.transform);
    }

}
