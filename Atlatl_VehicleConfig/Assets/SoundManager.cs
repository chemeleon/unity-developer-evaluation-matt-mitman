﻿using UnityEngine;
using System.Collections;

//air wrench sound from http://soundbible.com/1211-Air-Wrench.html
//fire truck from http://soundbible.com/669-Fire-Truck.html
//air wrench short from http://soundbible.com/1975-Air-Wrench-Short.html
/// <summary>
/// Connects sound effect playback to actions in Vehicle Configurator class
/// </summary>
public class SoundManager : MonoBehaviour {

    public AudioClip invalidClickSFX;
    public AudioClip partChangedSFX;
    public AudioClip partSelectedSFX;
    public AudioClip partUnselectedSFX;
    public AudioClip loadVehicleSFX;
    public AudioClip startDrivingSFX;

    AudioSource source;

    // Use this for initialization
    void Start () {
        VehicleConfigurator v = GameObject.Find("vehicle").GetComponent("VehicleConfigurator") as VehicleConfigurator;
        if (v != null)
        {
            v.OnInvalidClick += invalidClick;
            v.OnPartChange += partChanged;
            v.OnPartSelected += partSelected;
            v.OnPartUnselected += partUnselected;
            v.OnLoadVehicle += loadVehicle;
            v.OnStartVehicle += startDriving;
        }

        source = GetComponent<AudioSource>();
	}
	
    //the following functions are pretty obvious, just plays the matching sound
    void startDriving()
    {
        if (startDrivingSFX != null && source != null)
            source.PlayOneShot(startDrivingSFX, 1);
    }

    void invalidClick()
    {
        if (invalidClickSFX != null && source != null)
            source.PlayOneShot(invalidClickSFX);
    }

    void partChanged()
    {
        if (invalidClickSFX != null && source != null)
            source.PlayOneShot(partChangedSFX);
    }

    void partSelected()
    {
        if (invalidClickSFX != null && source != null)
            source.PlayOneShot(partSelectedSFX);
    }

    void partUnselected()
    {
        if (invalidClickSFX != null && source != null)
            source.PlayOneShot(partUnselectedSFX);
    }

    void loadVehicle()
    {
        if (invalidClickSFX != null && source != null)
            source.PlayOneShot(loadVehicleSFX);
    }
	
}
