﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Represents a single instance of a configurable element of the vehicle
/// </summary>
public class BodyPart : MonoBehaviour
{
    public CarParts partType; //the type of part
    public List<string> partPaths = new List<string>(); //all available parts we can pick from for this element
    int selectedIndex = 0; //currently selected index
    public VehicleConfigurator parentConfig; //reference to the vehicle configurator

    //linked parts let us have multiple unique pieces, that all get selected and changed together, ie 4 wheels
    public List<BodyPart> linkedParts = new List<BodyPart>(); 
    public bool canLink = true; //when passing calls to linked parts, set this false on each part until done so we dont get an infinite recursion

    public string hubPos = ""; //used to flag which corner of the vehicle we're at

    bool selected;
    /// <summary>
    /// Gets/Sets the selection state of the object, which includes a shader change to indicate if it's selected or not
    /// </summary>
    public bool Selected
    {
        get { return selected; }
        set
        {
            selected = value;

            //set the correct shader based on whether it's selected or not
            Renderer renderer = gameObject.GetComponent("Renderer") as Renderer;
            if (renderer != null)
            {
                if (selected)
                {
                    if(parentConfig.selectedShader != null)
                        renderer.material.shader = parentConfig.selectedShader;
                    renderer.material.SetColor("_RimColor", Color.green);
                }
                else
                {
                    if(parentConfig.standardShader != null)
                        renderer.material.shader = parentConfig.standardShader;
                }
            }

            if (canLink)
            {
                for (int i = 0; i < linkedParts.Count; i++)
                {
                    linkedParts[i].canLink = false;
                    linkedParts[i].Selected = value;
                    linkedParts[i].canLink = true;
                }
            }
        }
    }

    /// <summary>
    /// Gets/Sets the currently selected part choice for this element.  When the index is set, it loads up the new model and assigns material, etc
    /// </summary>
    public int SelectedIndex
    {
        get { return selectedIndex; }
        set
        {
            selectedIndex = value;

            //wrap the value if we're given something out of bounds
            if (selectedIndex >= partPaths.Count)
                selectedIndex = 0;
            else if (selectedIndex < 0)
                selectedIndex = partPaths.Count - 1;

            MeshFilter mf = gameObject.GetComponent("MeshFilter") as MeshFilter;
            if (mf != null)
            {
                string path = partPaths[SelectedIndex];
                Mesh mesh = (Mesh)Resources.Load(partPaths[SelectedIndex], typeof(Mesh));
                mf.mesh = mesh;

                Renderer renderer = gameObject.GetComponent("Renderer") as Renderer;
                if (renderer != null)
                {
                    //set material based on part name
                    if (path.Contains("Mil_1") && parentConfig.mil1Mat != null)
                        renderer.material = parentConfig.mil1Mat;
                    else if (path.Contains("Mil_2") && parentConfig.mil2Mat != null)
                        renderer.material = parentConfig.mil2Mat;
                    else if (path.Contains("Mil_3") && parentConfig.mil3Mat != null)
                        renderer.material = parentConfig.mil3Mat;
                }

                //make sure collision still fits this new mesh
                updateCollision();
            }
            Selected = Selected; //just reset selected state so selected shader stays visible since we just reassigned the material

            //pass this call onto linked body parts
            if (canLink)
            {
                for (int i = 0; i < linkedParts.Count; i++)
                {
                    //we set canlink to false, set the value, then reset it to true, so that body part doesnt recall it on all it's linked parts, causing an infinite loop
                    linkedParts[i].canLink = false;
                    linkedParts[i].SelectedIndex = value;
                    linkedParts[i].canLink = true;
                }
            }
        }
    }

    /// <summary>
    /// get the name of the currently selected part, without the rest of the resource path
    /// </summary>
    public string SelectedPartName
    {
        get
        {
            string part = partPaths[SelectedIndex];
            return part.Substring(part.LastIndexOf("/") + 1);
        }
    }

    /// <summary>
    /// setup the mesh collider for the currently active mesh
    /// </summary>
    public void updateCollision()
    {
        MeshCollider col = gameObject.GetComponent("MeshCollider") as MeshCollider;
        if (col == null) //missing, so create
            col = gameObject.AddComponent<MeshCollider>();

        MeshFilter meshFilter = gameObject.GetComponent("MeshFilter") as MeshFilter;
        if (meshFilter != null)
            col.sharedMesh = meshFilter.mesh;

        col.tag = "BodyPart";

        //pass on to linked parts
        if (canLink)
        {
            for (int i = 0; i < linkedParts.Count; i++)
            {
                linkedParts[i].canLink = false;
                linkedParts[i].updateCollision();
                linkedParts[i].canLink = true;
            }
        }
    }

    /// <summary>
    /// switch to the invalid mode, currently setting the rim light to red
    /// </summary>
    public void setInvalid()
    {
        Renderer renderer = gameObject.GetComponent("Renderer") as Renderer;
        if (renderer != null)
            renderer.material.SetColor("_RimColor", Color.red);

        //pass on to linked parts
        if (canLink)
        {
            for (int i = 0; i < linkedParts.Count; i++)
            {
                linkedParts[i].canLink = false;
                linkedParts[i].setInvalid();
                linkedParts[i].canLink = true;
            }
        }
    }
}
