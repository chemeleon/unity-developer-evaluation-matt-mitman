﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// holder class for part info for saving/loading
/// </summary>
public class PartData
{
    public int id;
    public CarParts partType;

    public PartData()
    {
    }

    public PartData(CarParts part, int i)
    {
        id = i;
        partType = part;
    }
}

/// <summary>
/// holds the overall config data for the entire vehicle when saving/loading
/// </summary>
public class ConfigData
{
    public string Name;
    public List<PartData> configIDs = new List<PartData>();// Dictionary<CarParts, int> configIDs = new Dictionary<CarParts, int>();
    
    /// <summary>
    /// finds the first ID that matches the given car part
    /// for now we're assuming that even if there are multiple parts with the same type, they'll all have the same ID
    /// </summary>
    public int getID(CarParts part)
    {
        for (int i = 0; i < configIDs.Count; i++)
            if (configIDs[i].partType == part)
                return configIDs[i].id;
        return -1;
    }
}
